# Hub

Simple command-line pub-sub program written in Haskell.

## Installation

1. Install [Stack](https://docs.haskellstack.org/en/stable/README/)
1. Clone this repo & `cd hub`
1. `stack install`

## Usage

To open a subscription

```shell
hub -s
```

To publish a message to subscribers

```shell
hub -p "Hello, World!"
```

## Help

```shell
hub -h
```

## Version info

```shell
hub -v
```
