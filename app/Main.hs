module Main where

import System.Environment
import System.Exit
import Subscribe
import Publish
import Subscribe

main :: IO ()
main = getArgs >>= parse

parse ["-s"] = subscribeMainChannel
parse ["-p", message] = publish(Just message) >> exit
parse ["-p"] = publish Nothing >> exit
parse ["-h"] = usage >> exit
parse ["-v"] = version >> exit
parse [] = usage

usage = putStrLn "Usage: hub [-psv] [text ..]"
version = putStrLn "hub v0.000000001"
exit = exitWith ExitSuccess
