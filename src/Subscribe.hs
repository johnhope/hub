module Subscribe
    ( subscribeMainChannel
    ) where

import System.IO
import System.Directory ( removeFile )
import System.Directory.Internal ( andM )
import System.Posix.Files ( createNamedPipe, unionFileModes, ownerReadMode, ownerWriteMode )
import System.Posix.Process ( getProcessID )
import System.Posix.Signals ( installHandler, Handler( CatchOnce ), sigINT )
import Control.Concurrent.MVar ( putMVar, newEmptyMVar, tryTakeMVar, MVar )

termHandler :: MVar () -> Handle -> Handler
termHandler v handle = CatchOnce $ do
    putStrLn "Dropping subscription and quitting..."
    hClose handle
    newChannelFileName >>= removeFile
    putMVar v ()

subscribeMainChannel :: IO ()
subscribeMainChannel = do
  putStrLn "Starting a new subscription. Ctrl-C to stop..."
  v <- newEmptyMVar
  channelName <- newChannelFileName
  createNamedPipe channelName $ unionFileModes ownerReadMode ownerWriteMode
  pipe <- openFile channelName ReadMode
  installHandler sigINT (termHandler v pipe) Nothing
  watchFileLine v pipe

watchFileLine :: MVar () -> Handle -> IO ()
watchFileLine v pipe = do
  val <- tryTakeMVar v -- used to check for signals
  case val of
    Just _ -> return ()
    Nothing -> do
      readFileLine pipe >>= putStr -- TODO: delay this, it's a hog. Also don't print null bytes
      watchFileLine v pipe

readFileLine :: Handle -> IO String
readFileLine pipe = do
  -- nothingToRead <- (||) <$> hIsClosed pipe <*> hIsEOF pipe -- WIP: OR doesn't seem to be greedy
  isClosed <- hIsClosed pipe
  if isClosed
  then return "\0"
  else do
    nothingToRead <- hIsEOF pipe
    if nothingToRead
    then return "\0" -- TODO: shitty hack. Just do nothing
    else (++) <$> hGetLine pipe <*> pure "\n"

newChannelFileName :: IO FilePath
newChannelFileName = do
  pid <- getProcessID
  return ("./hubsubpub-" ++ (show pid) ++ ".fifo")
