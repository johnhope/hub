module Publish
    ( publish
    ) where

import System.IO
import System.Directory
import Control.Monad

publish :: Maybe String -> IO ()
publish (Just message) = filterFifo <$> openFileNames >>= (publishToAll message)
publish Nothing = putStrLn "Can't publish empty message"

publishToAll :: String -> [FilePath] -> IO ()
publishToAll message a =
  mapM_ (publishToFile message) a

filterFifo :: [FilePath] -> [FilePath]
filterFifo = filter isFifo

isFifo :: FilePath -> Bool
isFifo filePath = (== "ofif.") $ take 5 $ reverse filePath -- I don't deserve the title 'programmer'

publishToFile :: String -> FilePath -> IO ()
publishToFile message filepath = do
  pipe <- openFile filepath WriteMode
  hPutStrLn pipe message
  hClose pipe

openFileNames :: IO [FilePath]
openFileNames = getCurrentDirectory >>= getDirectoryContents
